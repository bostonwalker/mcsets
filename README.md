# McSets
A barrel containing helper classes for working with sets in MonkeyC

## Usage
```
import Sets.Set;

var a = Set.from([1, 2, 2, 3]);
System.println(a.toString());
> {1, 2, 3}

var b = Set.from([3, 4]);

System.println(a.difference(b).toString());
> {1, 2}

System.println(a.union(b).toString());
> {1, 2, 3, 4}

System.println(a.intersection(b).toString());
> {3}
```

## License
McSets is available for use under the MIT license.
